/* clutter-gstreamermm - a C++ wrapper for clutter-gst
 *
 * Copyright 2007 The cluttermm Development Team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef LIBCLUTTER_GSTREAMERMM_H
#define LIBCLUTTER_GSTREAMERMM_H

/* cluttermm version.  */
extern const int cluttermm_major_version;
extern const int cluttermm_minor_version;
extern const int cluttermm_micro_version;

#include <cluttermm.h>
#include <gstreamermm.h>
#include <clutter-gstreamermm/init.h>
#include <clutter-gstreamermm/gst-audio.h>
#include <clutter-gstreamermm/gst-video-sink.h>
#include <clutter-gstreamermm/gst-video-texture.h>

#endif /* #ifndef LIBCLUTTER_GSTREAMERMM_H */
