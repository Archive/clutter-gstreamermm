#include <clutter-gstreamermm.h>
#include <iostream>
#include "videoapp.hh"

int main(int argc, char** argv)
{
  Glib::RefPtr<Clutter::Stage> stage;
  Glib::RefPtr<VideoApp> videoapp;
  Clutter::Color stage_color("black");

  if (argc < 2)
  {
    g_error("%s <video file>", argv[0]);
    return 1;
  }

  Clutter::init(&argc, &argv);
  Gst::init(argc, argv);

  stage = Clutter::Stage::get_default();
  stage->fullscreen();
  stage->set_color(stage_color);

  videoapp = VideoApp::create(argv[1]);

  stage->add_actor(videoapp->get_video_texture());
  stage->add_actor(videoapp);

  std::cout << "start" << std::endl;

  int x = (stage->get_width() - videoapp->get_width())/2;
  int y = stage->get_height() - (stage->get_height()/3);

  std::cout << Glib::ustring::compose("setting x = %1, y = %2, width = %2", x, y, videoapp->get_width()) << std::endl;

  videoapp->set_position(x, y);

  std::cout << "stop" << std::endl;

  videoapp->get_video_texture()->set_playing();

  stage->show();

  Clutter::main();

  return 0;
}
