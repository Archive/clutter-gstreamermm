#include <clutter-gstreamermm.h>

int main(int argc, char** argv)
{
  if(argc < 2)
  {
    g_error ("Usage: %s URI", argv[0]);
    return 1;
  }

  Glib::RefPtr<Clutter::Timeline> timeline;
  Glib::RefPtr<Clutter::Stage> stage;
  Glib::RefPtr<Clutter::Label> label;
  Clutter::Color stage_color(0xcc, 0xcc, 0xcc, 0xff);
  Glib::RefPtr<Clutter::Gst::Audio> audio;

  Clutter::init(&argc, &argv);
  Gst::init(argc, argv);

  stage = Clutter::Stage::get_default();
  stage->set_color(stage_color);

  // Make a label
  label = Clutter::Label::create();
  label->set_text("Music");
  label->set_position(100, 100);
  stage->add_actor(label);

  // Make a timeline, 100 frames, 30 fps
  timeline = Clutter::Timeline::create(100, 30);
  timeline->set_loop();

  // Set up audio player
  audio = Clutter::Gst::Audio::create();
  audio->set_uri(argv[1]);
  audio->set_playing();
  

  // Start the timeline
  timeline->start();
  stage->show_all();

  Clutter::main();

  return 0;
}
