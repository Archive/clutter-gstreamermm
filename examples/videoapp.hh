#include <clutter-gstreamermm.h>

class VideoApp : public Clutter::Group
{
public:
  virtual ~VideoApp();

  static Glib::RefPtr<VideoApp> create(const Glib::ustring&);
  Glib::RefPtr<Clutter::Gst::VideoTexture> get_video_texture();
  void show_controls(bool show=true);
  void toggle_pause_state();
  virtual bool on_input(Clutter::Event *event);

private:
  VideoApp(const Glib::ustring&);

protected:
  virtual void on_vtexture_size_change(int, int);
  virtual void on_controls_tl_new_frame(int);
  virtual void on_controls_tl_completed();
  virtual void on_effect_tl_new_frame(int);
  bool on_controls_timeout();
  virtual void on_notify();

  bool controls_showing, paused;
  sigc::connection controls_timer;
  Glib::RefPtr<Clutter::Gst::VideoTexture> video_texture;
  Glib::RefPtr<Gst::Element> sink;
  Clutter::Color control_color1, control_color2;
  Glib::RefPtr<Clutter::Texture> control_bg, control_play, control_pause;
  Glib::RefPtr<Clutter::Rectangle> control_seek1, control_seek2, control_seekbar;
  Glib::RefPtr<Clutter::Label> control_label;
  Glib::RefPtr<Clutter::Timeline> controls_tl, effect_tl;
};

