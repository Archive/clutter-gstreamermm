#include "videoapp.hh"
#include <iostream>

#define SEEK_H 20
#define SEEK_W 690

VideoApp::VideoApp(const Glib::ustring& filename)
: video_texture(Clutter::Gst::VideoTexture::create()),
  control_color1(73, 74, 77, 0xee),
  control_color2(0xcc, 0xcc, 0xcc, 0xff),
  control_bg(Clutter::Texture::create()),
  control_play(Clutter::Texture::create()),
  control_pause(Clutter::Texture::create()),
  control_seek1(Clutter::Rectangle::create(control_color1)),
  control_seek2(Clutter::Rectangle::create(control_color2)),
  control_seekbar(Clutter::Rectangle::create(control_color1)),
  control_label(Clutter::Label::create("Sans Bold 24", Glib::path_get_basename(filename), control_color1)),
  controls_tl(Clutter::Timeline::create(10, 30)),
  effect_tl(Clutter::Timeline::create(30, 90))
{
  video_texture->set_filename(filename);  
  video_texture->property_sync_size() = false;
  video_texture->signal_size_change().connect(sigc::mem_fun(*this, &VideoApp::on_vtexture_size_change));
  video_texture->property_position().signal_changed().connect(sigc::mem_fun(*this, &VideoApp::on_notify));

  sink = Clutter::Gst::VideoSink::create_element(video_texture);
  //sink->property_use_shaders() = true;

  control_bg->set_from_file("vid-panel.png");
  control_play->set_from_file("media-actions-start.png");
  control_pause->set_from_file("media-actions-pause.png");

  control_seekbar->set_opacity(0x99);

  add_actor(control_bg);
  add_actor(control_play);
  add_actor(control_pause);
  add_actor(control_seek1);
  add_actor(control_seek2);
  add_actor(control_seekbar);
  add_actor(control_label);

  control_play->set_position(30, 30);
  control_pause->set_position(30, 30);

  control_seek1->set_size(SEEK_W+10, SEEK_H+10);
  control_seek1->set_position(200, 100);

  control_seek2->set_size(SEEK_W, SEEK_H);
  control_seek2->set_position(205, 105);

  control_seekbar->set_size(0, SEEK_H);
  control_seekbar->set_position(205, 105);

  control_label->set_position(200, 40);

  controls_tl->signal_new_frame().connect(sigc::mem_fun(*this, &VideoApp::on_controls_tl_new_frame));
  controls_tl->signal_completed().connect(sigc::mem_fun(*this, &VideoApp::on_controls_tl_completed));

  effect_tl->signal_new_frame().connect(sigc::mem_fun(*this, &VideoApp::on_effect_tl_new_frame));

  Clutter::Stage::get_default()->signal_event().connect(sigc::mem_fun(*this, &VideoApp::on_input));

  set_opacity(0xee);
  hide();
}

VideoApp::~VideoApp()
{
}

Glib::RefPtr<VideoApp> VideoApp::create(const Glib::ustring& filename)
{
  return Glib::RefPtr<VideoApp>(new VideoApp(filename));
}

void VideoApp::show_controls(bool show)
{
  if(controls_tl->is_playing())
    return;

  if(show && !controls_showing)
  {
    controls_showing = true;
    controls_tl->start();

    sigc::slot<bool> my_slot = sigc::mem_fun(*this, &VideoApp::on_controls_timeout);
    sigc::connection connection = Glib::signal_timeout().connect(my_slot, 5000);
    controls_timer = connection;

    return;
  }

  if(show && controls_showing)
  {
    if(controls_timer)
  	{
  	  controls_timer.disconnect();

      sigc::slot<bool> my_slot = sigc::mem_fun(*this, &VideoApp::on_controls_timeout);
      sigc::connection connection = Glib::signal_timeout().connect(my_slot, 5000);
      controls_timer = connection;
  	}

    return;
  }

  if (!show && controls_showing)
  {
    controls_showing = false;
    controls_tl->start();
    return;
  }
}

void VideoApp::toggle_pause_state()
{
  if(paused)
  {
    video_texture->set_playing();
    paused = false;
    control_play->hide();
    control_pause->show();
  }
  else
  {
    video_texture->set_playing(false);
    paused = true;
    control_pause->hide();
    control_play->show();
  }
}

Glib::RefPtr<Clutter::Gst::VideoTexture> VideoApp::get_video_texture()
{
  return video_texture;
}

void VideoApp::on_vtexture_size_change(int width, int height)
{
  int new_x, new_y, new_width, new_height;
  Glib::RefPtr<Clutter::Stage> stage = Clutter::Stage::get_default();

  new_height = (height * stage->get_width()) / width;
  if (new_height <= stage->get_height())
  {
    new_width = stage->get_width();

    new_x = 0;
    new_y = (stage->get_height() - new_height) / 2;
  }
  else
  {
    new_width  = (width * stage->get_height()) / height;
    new_height = stage->get_height();

    new_x = (stage->get_width() - new_width) / 2;
    new_y = 0;
  }

  Glib::RefPtr<Clutter::Actor>::cast_dynamic(video_texture)->set_position(new_x, new_y);
  video_texture->set_size(new_width, new_height);
}

void VideoApp::on_controls_tl_new_frame(int frame_num)
{
  guint8 opacity;

  show_all();
  if(paused)
  {
    control_pause->hide();
    control_play->show();
  }
  else
  {
    control_pause->show();
    control_play->hide();
  }

  opacity = (frame_num * 0xde) / controls_tl->get_n_frames();

  if(!controls_showing)
    opacity = 0xde - opacity;
  
  set_opacity(opacity);
}

void VideoApp::on_controls_tl_completed()
{
  if(!controls_showing)
    hide_all();
}

void VideoApp::on_effect_tl_new_frame(int frame_num)
{
  video_texture->set_rotation(Clutter::Y_AXIS, frame_num * 12, CLUTTER_STAGE_WIDTH() / 2, 0, 0);
}

bool VideoApp::on_controls_timeout()
{
  show_controls(false);
  return false;
}

bool VideoApp::on_input(Clutter::Event *event)
{
  switch (event->type)
  {
    case CLUTTER_MOTION:
      show_controls();
      break;

    case CLUTTER_BUTTON_PRESS:
      if(controls_showing)
      {
        Glib::RefPtr<Clutter::Actor> actor;
        Clutter::ButtonEvent *bev = (ClutterButtonEvent*) event;

        actor = Clutter::Stage::get_default()->get_actor_at_pos(bev->x, bev->y);

        std::cout << Glib::ustring::compose("got actor %1 at pos %2x%3", actor, bev->x, bev->y) << std::endl;

        if(actor == control_pause || actor == control_play)
        {
          toggle_pause_state();
          return false;
        }

        if(actor == control_seek1 || actor == control_seek2 || actor == control_seekbar)
        {
          int x, y, dist, pos;

          control_seekbar->get_transformed_position(x, y);

          dist = bev->x - x;

          CLAMP(dist, 0, SEEK_W);

          pos = (dist * video_texture->get_duration()) / SEEK_W;

          Glib::RefPtr<Clutter::Media>::cast_dynamic(video_texture)->set_position(pos);
        }
      }
      break;

    case CLUTTER_KEY_RELEASE:
    {
      Clutter::KeyEvent* kev = (ClutterKeyEvent*) event;
  
      switch(Clutter::key_event_symbol(kev))
      {
        case CLUTTER_q:

        case CLUTTER_Escape:
          Clutter::main_quit();
          break;

        case CLUTTER_e:
          if(!effect_tl->is_playing())
            effect_tl->start();
          break;

        default:
          toggle_pause_state();
          break;
      }
    }

    default:
      break;
  }

  return false;
}

void VideoApp::on_notify()
{
  int w, h, position, duration, seek_w;
  gchar buf[256];

  position = Glib::RefPtr<Clutter::Media>::cast_dynamic(video_texture)->get_position();
  duration = video_texture->get_duration();

  if(duration == 0 || position == 0)
    return;

  control_seekbar->set_size((position * SEEK_W) / duration, SEEK_H);  
}

