#include <clutter-gstreamermm.h>

Glib::RefPtr<Clutter::Stage> stage;
Glib::RefPtr<Clutter::Texture> texture;

void on_size_change(int width, int height)
{
  int new_x, new_y, new_width, new_height;

  new_height = (height * stage->get_width()) / width;
  if(new_height <= stage->get_height())
  {
    new_width = stage->get_width();

    new_x = 0;
    new_y = (stage->get_height() - new_height) / 2;
  }
  else
  {
    new_width  = (width * stage->get_height()) / height;
    new_height = stage->get_height();

    new_x = (stage->get_width() - new_width) / 2;
    new_y = 0;
  }

  texture->set_position(new_x, new_y);

  texture->set_size(new_width, new_height);
}

int main(int argc, char** argv)
{
  if (argc < 1) {
          g_error ("Usage: %s", argv[0]);
          return 1;
  }

  Glib::RefPtr<Clutter::Timeline> timeline;
  Glib::RefPtr<Gst::Pipeline> pipeline;
  Glib::RefPtr<Gst::Element> src;
  Glib::RefPtr<Gst::Element> warp;
  Glib::RefPtr<Gst::Element> colorspace;
  Glib::RefPtr<Gst::Element> sink;

  Clutter::init(&argc, &argv);
  Gst::init(argc, argv);

  stage = Clutter::Stage::get_default();

  // Make a timeline, 100 frames, 30 fps
  timeline = Clutter::Timeline::create(100, 30);
  timeline->set_loop();

  // We need to set certain props on the target texture currently for
  // efficient/corrent playback onto the texture (which sucks a bit)
  texture = Clutter::Texture::create();
  texture->property_sync_size() = false;
  //texture->property_disable_slicing() = true;
  

  texture->signal_size_change().connect(sigc::ptr_fun(&on_size_change), texture);

  // Set up pipeline
  pipeline = Gst::Pipeline::create("video-sink");

  src = Gst::ElementFactory::create_element("videotestsrc");
  warp = Gst::ElementFactory::create_element("warptv");
  colorspace = Gst::ElementFactory::create_element("ffmpegcolorspace");
  sink = Clutter::Gst::VideoSink::create_element(texture);

  pipeline->add(src)->add(warp)->add(colorspace)->add(sink);
  src->link(warp)->link(colorspace)->link(sink);

  pipeline->set_state(Gst::STATE_PLAYING);

  timeline->start();

  stage->add_actor(texture);
  stage->show_all();

  Clutter::main();

  return 0;
}
